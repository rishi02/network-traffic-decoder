#include <stdio.h>
#include <iostream>
#include <stdint.h>
#include <time.h>
#include <sys/stat.h>
#include <string>
#include <string.h>
#include "util_functions.h"
#include "c_sslParser.h"

using namespace std;


//converts data from network byte order to host byte order(16 bit).
unsigned short int ntohl(unsigned short int const net){
	unsigned char data[2]={};
	memcpy(&data,&net,sizeof(data));
	return ((unsigned short int) data[1]<<0) | ((unsigned short int) data[0]<<8);
}


//converts data from network byte order to host byte order(32 bit).
unsigned int ntohl(unsigned int const net){
	unsigned char data[4]={};
	memcpy(&data,&net,sizeof(data));
	return (((unsigned int) data[3]<<0)
			| ((unsigned int) data[2]<<8) | ((unsigned int) data[1]<<16) | ((unsigned int) data[0]<<24));
}

//does directory exists.
uint8_t isDirExists(const char* dirPath){
	struct stat stats;
	stat(dirPath,&stats);

	//checking for existance.
	if(S_ISDIR(stats.st_mode)){
		return 1; 
	}
	return 0;
}

string getCurrentTime()
{
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[50];
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	//printf ( "Current local time and date: %s\n\n", asctime (timeinfo) );
	strftime (buffer,50,"%I_%M%p",timeinfo);
	return buffer;
}

//resets the all variables of the struct sslInfo for every packet.
void resetSslInfo(struct sslInfo* sslPacketInfo){
	sslPacketInfo->frag_or_not = false;
	sslPacketInfo->server_name_length = 0;
	sslPacketInfo->server_name = NULL;
}


//resets the all variables of the struct pcapPacketInfo for every packet.
void resetPcapPacketInfo(struct pcapPacketInfo* packetInfo){
	//Required structures.
	packetInfo->frame_header = NULL;
	packetInfo->ip_header = NULL;
	packetInfo->ip6_header = NULL;
	packetInfo->tcp_header = NULL;
	packetInfo->udp_header = NULL;

	//offset data variables.
	packetInfo->network_layer_offset = 0;
	packetInfo->transport_layer_offset = 0;
	packetInfo->tcp_payload_offset = 0;
	packetInfo->tcp_payload_length = 0;
	packetInfo->ip_version = 0;
	packetInfo->transport_protocol_number = 0;
}

//resets the all variables of the struct pcapStatistics.
void resetPcapStatistics(struct pcapStatistics* pcapStats){
	pcapStats->pcapPacketCounter = 0;
	pcapStats->ip4_count = 0;
	pcapStats->ip6_count = 0;
	pcapStats->tcp_count = 0;
	pcapStats->tcpV4_count = 0;
	pcapStats->tcpV6_count = 0;
	pcapStats->udp_count = 0;
	pcapStats->udpV4_count = 0;
	pcapStats->udpV6_count = 0;
	pcapStats->arp_count = 0;
	pcapStats->others_count = 0;
	pcapStats->vlan_count = 0;
}

//resets the all variables of the struct sslStatistics.
void resetSslStatistics(struct sslStatistics* sslStats){
	sslStats->client_hlw_count = 0;
	sslStats->cl_hlw_server_name_count = 0;
	sslStats->cl_hlw_no_server_name_count = 0;
	sslStats->cl_hlw_frag_server_name_count = 0;
	sslStats->server_hello_count = 0;
	sslStats->server_hello_with_cert_count = 0;
	sslStats->server_hello_with_no_cert_count = 0;
	sslStats->client_fragment_count = 0;
	sslStats->server_fragment_count = 0;
	sslStats->server_hello_cert_in_next_packet = 0;

}

