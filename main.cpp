#include <iostream>
#include <stdio.h>
#include <stdint.h>
#include <cstdlib>
#include <cstring>
#include <time.h>
#include <string>
#include<climits>
#include <dirent.h>
#include "c_sslParser.h"
#include "util_functions.h"

using namespace std;

/*************MACRO***************/


//Macros.
#define TCP 6
#define RECORD_LAYER_HEADER_LENGTH 5

int g_packet_count = 0;

/****END OF MACRO DEFINATIONS*****/



/*global struct declaration*/
struct pcapGlobalHeader global_header;
struct pcapPacketHeader packet_header;
struct pcapStatistics pcapStats; 
struct sslStatistics sslStats;

//struct initialisation for stroing packet Meta data.
struct pcapPacketInfo packetInfo;
struct sslInfo sslPacketInfo ;

//class object initialisation for parsing packets.
pcapPacketParser obj_parser;
sslParser obj_processSsl;

//reads global header and returns magic number.
uint32_t readGlobalHeader(FILE* fptr_currentPcapFile){
	
	//printf("******global header*******\n");
	fread(&global_header,sizeof(struct pcapGlobalHeader),1,fptr_currentPcapFile);
	return global_header.magic_number;
}


//csv writer function.
FILE* csvWriter(string pcapDumpFileName){
	FILE* fptr_clientHelloCsvDumpFile = NULL;

	fptr_clientHelloCsvDumpFile = fopen(pcapDumpFileName.c_str(),"w");

	if(fptr_clientHelloCsvDumpFile == NULL){
		printf("\nunable to open pcap file.\n");
		return NULL;
	}

	fprintf(fptr_clientHelloCsvDumpFile,"S.No,");
	fprintf(fptr_clientHelloCsvDumpFile,"Packet Number,");
	fprintf(fptr_clientHelloCsvDumpFile,"IP Address,");
	fprintf(fptr_clientHelloCsvDumpFile,"Port Number,");
	fprintf(fptr_clientHelloCsvDumpFile,"Server Name Length,");
	fprintf(fptr_clientHelloCsvDumpFile,"Server Name\n");

	return fptr_clientHelloCsvDumpFile;
}

//dump details to csv( true means ipv4 and false means ipv6).
void dumpClientHelloDetailsToCsv(FILE* fptr_clientHelloCsvDumpFile, struct sslInfo sslPacketInfo,struct pcapPacketInfo packetInfo){
	fprintf(fptr_clientHelloCsvDumpFile,"%u,",g_packet_count);
	fprintf(fptr_clientHelloCsvDumpFile,"%u,",pcapStats.pcapPacketCounter);

	if(packetInfo.ip_version == 4){
		unsigned char* ip_addr = packetInfo.ip_header->ip_dst;
		fprintf(fptr_clientHelloCsvDumpFile,"%u.%u.%u.%u,",ip_addr[0],ip_addr[1],ip_addr[2],ip_addr[3]);
		
	}else if(packetInfo.ip_version == 6 ){
		
		unsigned char* ip6_addr = packetInfo.ip6_header->dst;
		fprintf(fptr_clientHelloCsvDumpFile,"%x%x::%x%x::%x%x::%x%x::%x%x::%x%x::%x%x::%x%x,",
						       ip6_addr[0],ip6_addr[1],ip6_addr[2],ip6_addr[3],
					           ip6_addr[4],ip6_addr[5],ip6_addr[6],ip6_addr[7],
						     ip6_addr[8],ip6_addr[9],ip6_addr[10],ip6_addr[11],
						  ip6_addr[12],ip6_addr[13],ip6_addr[14],ip6_addr[15]);
	}

	if(packetInfo.transport_protocol_number == TCP) {
		fprintf(fptr_clientHelloCsvDumpFile,"%u,",ntohl(packetInfo.tcp_header->tcph_destport));
	}

	fprintf(fptr_clientHelloCsvDumpFile,"%u,",sslPacketInfo.server_name_length);
	fprintf(fptr_clientHelloCsvDumpFile,"%*s\n",sslPacketInfo.server_name_length, sslPacketInfo.server_name);

	g_packet_count += 1;
}


//keep count of ssl packets.
void updateCountOfSSLPackets(int statusCode, struct sslInfo sslPacketInfo){
	
	switch(statusCode){
		case 0 : {	sslStats.cl_hlw_no_server_name_count++;
				 sslStats.client_hlw_count++;
				 break;
			 }

		case 1 : {	sslStats.cl_hlw_frag_server_name_count++;
				 sslStats.client_hlw_count++;
				 break;
			 }

		case 2 : {	sslStats.cl_hlw_server_name_count++;
				 sslStats.client_hlw_count++;
				 break;
			 }

		case 3 : {	sslStats.server_hello_with_cert_count++;
				 sslStats.server_hello_count++;
				 break;
			 }

		case 4 : {	sslStats.server_hello_with_no_cert_count++;
				 sslStats.server_hello_count++;
				 break;
			 }
		
		case 5 : {	sslStats.server_hello_cert_in_next_packet++;
					sslStats.server_hello_count++;
					break;
			}
		
		default : {
				  break;
				}
	}

}


//print summary.
void printSummary(){
	printf("\n\n************************SUMMARY****************************\n\n");
	printf("total packet Count							: %u\n",pcapStats.pcapPacketCounter);
	printf("IPv4 Count								: %u\n",pcapStats.ip4_count);
	printf("Ipv6 count								: %u\n\n",pcapStats.ip6_count);

	printf("tcp count								: %u\n",pcapStats.tcp_count);
	printf("tcp in ipv4 count							: %u\n",pcapStats.tcpV4_count);
	printf("tcp in ipv6 count							: %u\n\n",pcapStats.tcpV6_count);

	printf("udp count								: %u\n",pcapStats.udp_count);
	printf("udp in ipv4 count							: %u\n",pcapStats.udpV4_count);
	printf("udp in ipv6  count							: %u\n\n",pcapStats.udpV6_count);

	printf("arp count								: %u\n",pcapStats.arp_count);
	printf("VLAN count								: %u\n",pcapStats.vlan_count);
	printf("others Count								: %u\n\n",pcapStats.others_count);

	printf("Total Client Hello Packets Count					: %u\n",sslStats.client_hlw_count);
	printf("Client Hello with no server Name Count					: %u\n",sslStats.cl_hlw_no_server_name_count);
	printf("Client hello with partial server name					: %u\n",sslStats.cl_hlw_frag_server_name_count);
	printf("Client hello with server name count					: %u\n\n",sslStats.cl_hlw_server_name_count);

	printf("Total Server Hello packets count					: %u\n",sslStats.server_hello_count);
	printf("Server Hello packets with certificate count				: %u\n",sslStats.server_hello_with_cert_count);
	printf("Server Hello packets with certificate in next packet count		: %u\n",sslStats.server_hello_cert_in_next_packet);
	printf("Server Hello packets with no certificate count				: %u\n\n",sslStats.server_hello_with_no_cert_count);
}


//Pcap File Processor.
int processPcapFile(string pcapDirPath , string fileName){

	//Pcap File pointer
	FILE*  fptr_currentPcapFile =  NULL;
	
	//returns nt pointer to the pcap file.
	fptr_currentPcapFile = fopen((pcapDirPath +  fileName).c_str(),"rb");
	
	if(fptr_currentPcapFile == NULL){
		printf("\nunable to process %s \n",fileName.c_str());
		return -1;
	}

	//reads global header.
	uint32_t magicNumber = readGlobalHeader(fptr_currentPcapFile);

	uint32_t littleEndian = 0xa1b2c3d4 , bigEndian = 0xd4c3b2a1;

	if(magicNumber != littleEndian && magicNumber != bigEndian){
		printf("\nUnsupported fileType......Check the above magic number for reference\n");
		return -1;
	}
	
	//char buffer 
	unsigned char*  buff = (unsigned char*)malloc(global_header.snapLength*sizeof(char));
	
	//creates a dynamic name for fptr_clientHelloCsvDumpFile.
	string pcapDumpFileName = fileName.substr(0,fileName.find_last_of(".")) + "_ClientHelloDump["+getCurrentTime()+"].csv";
	
	FILE* fptr_clientHelloCsvDumpFile = csvWriter(pcapDumpFileName);
	
	//loops on all packets of pcap.
	while(1){
		
		//resets all values of struct variables to default values.
		resetSslInfo(&sslPacketInfo);
		resetPcapPacketInfo(&packetInfo);
		
		//reading packet header.
		fread(&packet_header,sizeof(struct pcapPacketHeader),1,fptr_currentPcapFile);

		//loads the whole packet data into the buffer.
		fread(buff , packet_header.incl_len , 1 , fptr_currentPcapFile);	

		//checks for the end of the file.
		if(feof(fptr_currentPcapFile)){
			printf("\nTotal Packet Count %d\n",pcapStats.pcapPacketCounter);
			printf("\nEOF Reached\n");
			obj_processSsl.clearSet();
			free(buff);

			printSummary();
			return 1;
		}

		++pcapStats.pcapPacketCounter;
		
		int transport_protocol = obj_parser.parsePacket(buff,packet_header.incl_len,&packetInfo,&pcapStats);

		//printf("packet Number : %u\n",pcapStats.pcapPacketCounter);
		int packetStatus = 0;

		if(transport_protocol == TCP && packetInfo.tcp_payload_length > RECORD_LAYER_HEADER_LENGTH ){
			packetStatus = obj_processSsl.parseSslPacket(&buff[packetInfo.tcp_payload_offset],packetInfo.tcp_payload_length,&sslPacketInfo,&packetInfo);
			if(fptr_clientHelloCsvDumpFile != NULL && packetStatus == 2){
				dumpClientHelloDetailsToCsv(fptr_clientHelloCsvDumpFile,sslPacketInfo,packetInfo);
			}
			updateCountOfSSLPackets(packetStatus,sslPacketInfo);
		}
	}
}


//Main function.
int main(int argc, char* argv[]){
	
	if(argc < 2){
		cout<<"\nNo command Line argument given ..Enter the pcap folder path.\n";
		return -1;
	}
	
	char* path = argv[1];
	
	//checks directory exists.
	if(isDirExists(path) == 0){
		cout<<"\nfolder does not exists.\n";
		return -1;
	}
	
	struct dirent *entry;
	DIR *dir = opendir(path);

	string pcapDirPath = path;
	if(pcapDirPath.substr(pcapDirPath.find_last_of("/")) != "/"){
		pcapDirPath += "/";
	}
	
		/*(entry->d_name)*/
		int countPcapFiles = 0;
		while ((entry = readdir(dir)) != NULL) {
			
			string fileName = entry->d_name;
			if(fileName.substr(fileName.find_last_of(".")+1)=="pcap"){
				cout<<"\nFile Name : "<<fileName<<endl;
				
				//initialisation of the pcap file data.
				g_packet_count = 1;
				resetPcapStatistics(&pcapStats);
				resetSslStatistics(&sslStats);
				int isFileProcessed = processPcapFile(pcapDirPath, fileName);
				
				if(isFileProcessed == 1){
						printf("\nPcap file : %s processed.\n",fileName.c_str());
				}
				
				countPcapFiles++;
				cout<<endl<<endl;	
			}
		}
		
		//checks if directory is empty.
		if(countPcapFiles ==0){
			cout<<"\nFolder contains 0 pcap files.\n";
		}
	closedir(dir);
	
	return 0;
}
