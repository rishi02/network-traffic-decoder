#ifndef UTIL_FUNCTIONS_H
#define UTIL_FUNCTIONS_H


//struct used for storing ssl packet Stats for file.
struct sslStatistics{
	int client_hlw_count;
	int cl_hlw_server_name_count;
	int cl_hlw_no_server_name_count;
	int cl_hlw_frag_server_name_count;
	int server_hello_count;
	int server_hello_with_cert_count;
	int server_hello_with_no_cert_count;
	int client_fragment_count;
	int server_fragment_count;
	int server_hello_cert_in_next_packet;
}__attribute__((packed));


/*Utility Functions.*/

//takes pcaps file path as input and valiadte.
FILE* getFilePathAndValidate(char* filePath);

//converts data from network byte order to host byte order(16 bit).
unsigned short int ntohl(unsigned short int const net);


//converts data from network byte order to host byte order(32 bit).
unsigned int ntohl(unsigned int const net);

//verify if directory exists.
uint8_t isDirExists(const char* dirPath);

//returns a string containing the current time in UTC(GMT).
std :: string getCurrentTime();

void resetSslInfo(struct sslInfo* sslPacketInfo);

void resetPcapPacketInfo(struct pcapPacketInfo* packetInfo);

void resetPcapStatistics(struct pcapStatistics* pcapStats);

void resetSslStatistics(struct sslStatistics* sslStats);

#endif