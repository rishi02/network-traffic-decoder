#ifndef NETWORK_HEADERS_H
#define NETWORK_HEADERS_H

#include <stdint.h>

//global header
struct pcapGlobalHeader {
	uint32_t magic_number;   	/* magic number */
	uint16_t version_major;  	/* major version number */
	uint16_t version_minor;  	/* minor version number */
	int32_t  thiszone;      	/* GMT to local correction */
	uint32_t sigfigs;       	/* accuracy of timestamps */
	uint32_t snapLength;        /* max length of captured packets, in octets */
	uint32_t networkType;       /* data link type */
};

//packet header
struct pcapPacketHeader {
	uint32_t ts_sec;         	/* timestamp seconds */
	uint32_t ts_usec;        	/* timestamp microseconds */
	uint32_t incl_len;       	/* number of octets of packet saved in file */
	uint32_t orig_len;       	/* actual length of packet */
};

//ethernet header
struct ethernet_hdr_s {
	uint8_t dst[6];    			/* destination host address */
	uint8_t src[6];    			/* source host address */
	uint16_t type;     			/* IP? ARP? RARP? etc */
};

//ipv4 header
struct ip_hdr_s {
	uint8_t  ip_hl:4, 						/* both fields are 4 bits */
		 ip_v:4;  						/* ip version*/
	uint8_t        typeOfService;			/*type of service*/
	uint16_t       ipPacketLength;			/*length of ip packet*/
	uint16_t       identificationNumber;	/*identification number*/
	uint16_t       fragmntOffset;			/*fragment offset*/
	uint8_t        timeToLive;				/*time to live*/
	uint8_t        nextProtocolHeader;		/*Protocol Number*/
	uint16_t    	headerChecksum;			/*checksum*/
	unsigned char ip_src[4];				/*source ip address*/
	unsigned char ip_dst[4];				/*destination ip address*/
};

//ipv6 header
struct ipv6_header
{
	uint32_t firstRow;				/*contains ip version , priority/traffic class  and flow label */
	uint16_t length;				/*payload length*/
	uint8_t  nextProtocolHeader;	/*next protocol header*/
	uint8_t  hopLimit;				/*time to live / hop limit*/
	unsigned char src[16];			/*ipv6 source address*/
	unsigned char dst[16];			/*ipv6 destination address*/
};

/* Structure of a TCP header */
struct tcpheader {
	uint16_t tcph_srcport;			/*source port*/
	uint16_t tcph_destport;			/*destination port*/
	uint32_t       tcph_seqnum;		/*sequence number*/
	uint32_t       tcph_acknum;		/*acknowledgement number*/
	uint16_t
		reserveBits1:4,       		/*little-endian(reserve  bits)*/
		headerLength:4,     		/*length of tcp header in 32-bit words*/	
		finFlag:1,       			/*Finish flag "fin"*/
		synFlag:1,       			/*Synchronize sequence numbers to start a connection*/
		rstFlag:1,       			/*Reset flag */
		pushFlag:1,      			/*Push, sends data to the application*/
		ackFlag:1,       			/*acknowledgement bit*/
		urgentFlag:1,       		/*urgent pointer*/
		reserveBits2:2;				/*rserve bits*/
	uint16_t windowSize;			/*advertisement window*/
	uint16_t headerChecksum;		/*header checksum*/
	uint16_t urgentPointer;			/*urgent pointer*/
};

//udp header
struct udpheader{
	uint16_t src;		/*source port*/
	uint16_t dst;		/*destination port*/
	uint16_t length;	/*udp packet length including header*/
	uint16_t checksum;	/*checksum*/
};


//packet Information struct for packet
struct pcapPacketInfo{

	//Required structures.
	struct ethernet_hdr_s* frame_header;
	struct ip_hdr_s* ip_header;
	struct ipv6_header* ip6_header;
	struct tcpheader* tcp_header;
	struct udpheader* udp_header;

	//offset data variables.
	uint16_t network_layer_offset;
	uint16_t transport_layer_offset;
	uint16_t tcp_payload_offset;
	uint16_t tcp_payload_length;
	uint8_t  ip_version;
	uint8_t transport_protocol_number;
}__attribute__((packed));

#endif