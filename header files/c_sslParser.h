#ifndef SSLPARSER_H
#define SSLPARSER_H

#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <stdint.h>
#include <cstring>
#include <string.h>
#include <set>
#include "network_headers.h"
#include "c_pcapPacketParser.h"

using namespace std;

extern set<string> g_session_id_set;

/*
 * Struct sslInfo 
 *-----------------------------------------------
 * Summary : Contains the details of each TLS layer packet.
 */
struct sslInfo{	
	bool frag_or_not;
	uint16_t server_name_length;
	unsigned char* server_name;
}__attribute__((packed));


/*
 *Class : sslParser
 *----------------------------------------------------
 *Summary :	This is c_sslParser.h file which will be included
 *			in any main function before use to parse the 
 *			security layer of the iso/osi model.
 */
 
class sslParser{
	private:
	
   /*
	*Member variables
	*---------------------------------------------------
	* buff_ptr : It is the Char pointer to the buffer containing
	*			 the Captured bytes of the packets.
	*
	* bytesRead : It is the variable which will decide the byte number
	*			  of the buffer to be read.
	*
	* ssl_payload_length : It is the length which is specified in the tcp
	* 					   payload, and specifies number of bytes to be read
	*					   in the tls layer.
	*/
		unsigned char* m_buff_ptr;
		int m_bytesRead ; 
		uint16_t m_ssl_payload_length;
	
			
   /*
	*Utility Functions
	*---------------------------------------------------
	* parseClientHello : Parses the Client hello packet .
	*/
		int parseClientHello(struct sslInfo*);
		
   /*
	*Utility Functions
	*---------------------------------------------------
	* parseServerHello : Parses the Server hello packet .
	*/
		int parseServerHello(struct sslInfo* , struct pcapPacketInfo*);
		
	/*
	*Utility Functions
	*---------------------------------------------------
	* createSessionId : Creates ip-port pair(session id) for required packets .
	*/	
		void createSessionId(string* session_id,struct pcapPacketInfo* packetInfo);
		
		
	public:
	
	/*
	*Utility Functions
	*---------------------------------------------------
	* clearSet : clears the content of the set at the end of file.
	*/	
		void clearSet();
	
	
	   /*
		* Interface parseSslPacket
		*---------------------------------------------------
		* Summary : interface of the class, through which user can pass values.
		*/
		int parseSslPacket(unsigned char* buff , int payloadLength , struct sslInfo*, struct pcapPacketInfo*);
			
};

#endif