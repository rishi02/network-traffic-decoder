#ifndef PCAPPACKETPARSER_H
#define PCAPPACKETPARSER_H

#include <iostream>
#include <stdint.h>
#include "network_headers.h"




//pcap statistics for file
struct pcapStatistics{
	int pcapPacketCounter;
	int ip4_count;
	int ip6_count;
	int tcp_count;
	int tcpV4_count;
	int tcpV6_count;
	int udp_count;
	int udpV4_count;
	int udpV6_count;
	int arp_count;
	int others_count;
	int vlan_count;
}__attribute__((packed));


/*
 *Class : pcapPacketParser
 *----------------------------------------------------
 *Summary :	This is c_pcapPacketrParser.h file which will be included
 *			in any main function before use to parse the 
 *			packet upto tranport layer of the iso/osi model.
 */
class pcapPacketParser{
	private : 
		//int variables.
		int m_bytesRead;
		int m_capturedFrameLength;
		unsigned char*  m_buffPtr = NULL;

		//functions.
		std:: pair<bool,uint16_t> parseEthernet(struct pcapPacketInfo* packetInfo);
		uint16_t handleEthernetTunneling(uint16_t ether_type); 
		std :: pair<bool,uint8_t> parseIpv4(struct pcapPacketInfo* packetInfo,struct pcapStatistics*);
		std :: pair<bool,uint8_t> parseIpv6(struct pcapPacketInfo* packetInfo,struct pcapStatistics*);
		uint8_t handleNetworkLayerTunneling(uint8_t protocol_number);
		int parseTcp(struct pcapPacketInfo* ,uint16_t ip_payload_len , struct pcapStatistics*);
		int parseUdp(struct pcapPacketInfo* ,struct pcapStatistics*);

	public :

		//interface.
		int parsePacket(unsigned char*  buff , int buffLength , struct pcapPacketInfo* , struct pcapStatistics*);
};


#endif