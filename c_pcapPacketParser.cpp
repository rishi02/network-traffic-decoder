#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "c_pcapPacketParser.h"
#include "util_functions.h"

#define ARP		0x0806
#define IPV4 	0x800
#define IPV6 	0x86dd
#define IPV6_HEADER_LEN		40
#define IPV6_EXTEN_HEADER_LENGTH 	0x08
#define TCP 	0x06
#define UDP 	0x11 
#define ENCAP_IPV4 		0x04
#define ENCAP_IPV6 	 	0x29
#define IPV6_FRAG_HEADER 	0x2c
#define	IPV6_EXTEN_HEADER 	0x00
#define IEEE_802_1Q		0x8100
#define IEEE_802_1Q_TAG_LENGTH		0x04
#define UNKNOWN_PROTOCOL 	254




//parse ethernet packet.
std::pair<bool,uint16_t>  pcapPacketParser :: parseEthernet(struct pcapPacketInfo* packetInfo){

	if(m_capturedFrameLength >= sizeof(struct ethernet_hdr_s) && m_bytesRead <= m_capturedFrameLength){

		packetInfo->frame_header = (struct ethernet_hdr_s*)(m_buffPtr);

		//skipping ethernet.
		m_bytesRead += sizeof(struct ethernet_hdr_s);

		uint16_t l_ether_type = ntohl(packetInfo->frame_header->type);

		//handles ethernet tunneling.
		l_ether_type = handleEthernetTunneling(l_ether_type);

		//setting network layer offset.
		packetInfo->network_layer_offset = m_bytesRead;

		return std::pair<bool,uint16_t>(true,l_ether_type);
	}
	return std::pair<bool,uint16_t>(false,0);
}



//handles layer 2 tunneling.
uint16_t pcapPacketParser ::  handleEthernetTunneling(uint16_t ether_type){
	while(1){
		switch(ether_type){
			
			case IEEE_802_1Q : {
									ether_type = (uint16_t)m_buffPtr[m_bytesRead+2]<<8 | (uint16_t)m_buffPtr[m_bytesRead+3]<<0;
					
									//skips the IEEE 802.1Q(VLAN) Tag.
									m_bytesRead += IEEE_802_1Q_TAG_LENGTH;
									break;	
							}

			default			 : {
									return ether_type;
							}
		}
	}
}

//parse ipv4 packets.
std::pair<bool,uint8_t> pcapPacketParser :: parseIpv4(struct pcapPacketInfo* packetInfo,struct pcapStatistics* pcapStats){
	
	if(m_bytesRead <= m_capturedFrameLength ){
		
		packetInfo->ip_header = (struct ip_hdr_s*)(&m_buffPtr[m_bytesRead]);
		
		//calculates fragment offset using bit shifting.
		uint16_t fragmentOffset = ntohl(packetInfo->ip_header->fragmntOffset)<<3;
		fragmentOffset = fragmentOffset>>3;

		//update ipv4 count.
		pcapStats->ip4_count++;
	
		//updates in struct that this packet is ipv4.	
		packetInfo->ip_version = 4;
		
		//checks for ipversion and fragmentation status.
		if(packetInfo->ip_header->ip_v == 4 && fragmentOffset == 0){	
			
			//skipping ipv4 header with options field.	
			m_bytesRead += (packetInfo->ip_header->ip_hl*4);

			uint8_t l_protocol_number = packetInfo->ip_header->nextProtocolHeader;
			
			//resolve next header from tunneling.
			l_protocol_number = handleNetworkLayerTunneling(l_protocol_number);	
			
			//setting up transport layer offset.
			packetInfo->transport_layer_offset = m_bytesRead;

			//returning the next protocol type.
			return std::pair<bool,uint8_t>(true,l_protocol_number);
			
		}else if(packetInfo->ip_header->ip_v == 4 && fragmentOffset != 0){
				//this packet is fragmented.
				
				return std::pair<bool,uint8_t>(true,UNKNOWN_PROTOCOL);
		}
	}
	return std::pair<bool,uint8_t>(false,UNKNOWN_PROTOCOL);
}


//parse ipv6 packets.
std::pair<bool,uint8_t> pcapPacketParser :: parseIpv6(struct pcapPacketInfo* packetInfo,struct pcapStatistics* pcapStats){

	if(m_bytesRead <= m_capturedFrameLength){
		packetInfo->ip6_header = (struct ipv6_header*)(&m_buffPtr[m_bytesRead]);
	
		//updates in struct that this packet is ipv6.	
		packetInfo->ip_version = 6;

		//checks if ip version is 6.
		if(ntohl(packetInfo->ip6_header->firstRow)>>28 == 6){
			pcapStats->ip6_count++;

			//skipping ipv6 header.
			m_bytesRead += sizeof(struct ipv6_header);
			
			//saves the protocol number of the next header.
			uint8_t l_protocol_number = packetInfo->ip6_header->nextProtocolHeader;
			
			//handles network layer tunneling. 
			l_protocol_number = handleNetworkLayerTunneling(l_protocol_number);	

			//setting up transport layer.
			packetInfo->transport_layer_offset = m_bytesRead;

			return std::pair<bool,uint8_t>(true,l_protocol_number);
		}

	}
				
	return std::pair<bool,uint8_t>(false,UNKNOWN_PROTOCOL);
}



//handles layer 3 tunneling (also skips ipv6 extension headers).
uint8_t pcapPacketParser ::  handleNetworkLayerTunneling(uint8_t protocol_number){
	
	while(1){
		
		switch(protocol_number){
			
			case ENCAP_IPV4 		: {	//identifies next header if its IPV4.
										protocol_number = m_buffPtr[m_bytesRead+9];
					
										//skips the IPV4 header by reading header length.
										m_bytesRead += (m_buffPtr[m_bytesRead])*4;

										break;	
									}

			case ENCAP_IPV6 		: {	//identifies next header if its IPV6.
										protocol_number = m_buffPtr[m_bytesRead+6];
					
										//skips the IPV6 header..
										m_bytesRead += IPV6_HEADER_LEN;

										break;	
									} 

			case IPV6_FRAG_HEADER : {	//identifies if its ipv6 fragment header.
										protocol_number = m_buffPtr[m_bytesRead];
				
										uint8_t ipv6_more_frag_bit =  m_buffPtr[m_bytesRead+3]<<7; 
										ipv6_more_frag_bit >> 7;

										if(ipv6_more_frag_bit != 0){
											protocol_number = UNKNOWN_PROTOCOL;
										}	
									
										//skips ipv6 fragment header.
										m_bytesRead +=8;
					
										break;
									}	
			
			case IPV6_EXTEN_HEADER : {	//skips ipv6 extension header.
										protocol_number = m_buffPtr[m_bytesRead];
				
										//skips extension header.
										m_bytesRead += IPV6_EXTEN_HEADER_LENGTH;
					
										break;
									}
									
			default 				: {
										return protocol_number;
									}
		}
	}
}



//parse TCP packets.
int pcapPacketParser :: parseTcp(struct pcapPacketInfo* packetInfo,uint16_t l_ip_payload_len,struct pcapStatistics* pcapStats){

	if(m_bytesRead <= m_capturedFrameLength){
		packetInfo->tcp_header = (struct tcpheader*)(&m_buffPtr[m_bytesRead]);

		pcapStats->tcp_count++;

		packetInfo->transport_protocol_number = TCP;

		if(packetInfo->ip_version ==4){
			pcapStats->tcpV4_count++;
		}else if(packetInfo->ip_version == 6){
			pcapStats->tcpV6_count++;
		}

		//skipping tcp header with options field.
		m_bytesRead +=  (packetInfo->tcp_header->headerLength*4);

		//setting up SSL offset.
		packetInfo->tcp_payload_offset = m_bytesRead;

		//finding the tcp payload length.
		packetInfo->tcp_payload_length = l_ip_payload_len - (packetInfo->tcp_header->headerLength*4);

		//returns protocol number number of TCP.
		return TCP;
	}
	return -1;
}

//parse UDP packets.
int  pcapPacketParser :: parseUdp(struct pcapPacketInfo* packetInfo,struct pcapStatistics* pcapStats){

	if(m_bytesRead <= m_capturedFrameLength){
		if(packetInfo->ip_version == 4){
			pcapStats->udpV4_count++;
		}else if(packetInfo->ip_version == 6){
			pcapStats->udpV6_count++;
		}

		packetInfo->udp_header = (struct udpheader*)(&m_buffPtr[m_bytesRead]);

		packetInfo->transport_protocol_number = UDP;

		pcapStats->udp_count++;		

		//returns the protocol number of the UDP.
		return UDP;
	}

	return -1;
}

//parse pcap packet. 
int pcapPacketParser :: parsePacket(unsigned char*  buff , int buffLength , struct pcapPacketInfo* packetInfo,struct pcapStatistics* pcapStats){
	
	//sets the member variables.
	m_buffPtr = buff;
	m_capturedFrameLength = buffLength;
	m_bytesRead = 0;

	//Data Link Layer.
	std::pair<bool,uint16_t> ether_pair = parseEthernet(packetInfo);	

	if(ether_pair.first == true){

		uint16_t l_ether_type  = ether_pair.second;

		//Network Layer.
		uint8_t l_nextHeader = 0;
		bool l_isValid = false;
		uint16_t l_ip_payload_len = 0;


		if(l_ether_type == IPV4 ){
			std::pair<bool,uint8_t> ipv4_pair = parseIpv4(packetInfo,pcapStats);
			l_nextHeader = ipv4_pair.second;
			l_isValid = ipv4_pair.first;

			//stores the ip payload length.	
			l_ip_payload_len = ntohl(packetInfo->ip_header->ipPacketLength) - (packetInfo->ip_header->ip_hl*4);

		}else if(l_ether_type == IPV6)  {
			std::pair<bool,uint8_t> ipv6_pair = parseIpv6(packetInfo,pcapStats);
			l_nextHeader = ipv6_pair.second;
			l_isValid = ipv6_pair.first;

			//stores the ipv6 payload length.
			l_ip_payload_len = ntohl(packetInfo->ip6_header->length);

		}else if(l_ether_type ==  ARP){
			pcapStats->arp_count++;
			return 1;
		}else{
			pcapStats->others_count++;
			return 1;
		}

		//Transport Layer.
		if(l_nextHeader == TCP && l_isValid == true){
			return parseTcp(packetInfo,l_ip_payload_len,pcapStats);	
		}else if(l_nextHeader == UDP && l_isValid == true){
			return parseUdp(packetInfo,pcapStats);
		}
	}
	return -1;
}
