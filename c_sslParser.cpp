#include <stdio.h>
#include<string>
#include<string.h>
#include<climits>
#include<iterator>
#include<set>
#include "network_headers.h"
#include "c_sslParser.h"
#include "c_pcapPacketParser.h"



#define HANDSHAKE 0x16
#define CLIENT_HELLO 0x01
#define SERVER_HELLO 0x02
#define CERTIFICATE 0x0b
#define IPV4 4
#define IPV6 6
#define TLS_V1_0 0x0300
#define TLS_V1_1 0x0301
#define TLS_V1_2 0x0302
#define TLS_V1_3 0x0303
#define IP_ADDRESS_LENGTH 50
#define PORT_LENGTH 10

#define VERIFY_CURRENT_BYTE_OF_PACKET()({ 		\
	if(m_bytesRead >= m_ssl_payload_length){	\
		return -1;								\
	}											\
})												

#define _VERIFY_NEXT_BYTE_OF_PACKET(x)({		\
	int temp = m_bytesRead + (x);				\
	if(temp >= m_ssl_payload_length){			\
		return -1;								\
	}											\
	temp;										\
})

/* GLOBAL DATA STRUCTURE */
set<string> g_session_id_set;


//takes struct and returns session id(ip+port).
void sslParser :: createSessionId(string* session_id, struct pcapPacketInfo* packetInfo){

	char src_ip[IP_ADDRESS_LENGTH],dst_ip[IP_ADDRESS_LENGTH];

	//stores ip addresses in char array.
	if(packetInfo->ip_version == IPV4){
		sprintf(src_ip,"%u.%u.%u.%u",packetInfo->ip_header->ip_src[0],packetInfo->ip_header->ip_src[1],
									 packetInfo->ip_header->ip_src[2],packetInfo->ip_header->ip_src[3]);
									 
		sprintf(dst_ip,"%u.%u.%u.%u",packetInfo->ip_header->ip_dst[0],packetInfo->ip_header->ip_dst[1],
									 packetInfo->ip_header->ip_dst[2],packetInfo->ip_header->ip_dst[3]);
		
	}else if(packetInfo->ip_version == IPV6){
		sprintf(src_ip,"%x%x:%x%x:%x%x:%x%x:%x%x:%x%x:%x%x:%x%x", packetInfo->ip6_header->src[0],packetInfo->ip6_header->src[1],
																  packetInfo->ip6_header->src[2],packetInfo->ip6_header->src[3],
																  packetInfo->ip6_header->src[4],packetInfo->ip6_header->src[5],
																  packetInfo->ip6_header->src[6],packetInfo->ip6_header->src[7],
																  packetInfo->ip6_header->src[8],packetInfo->ip6_header->src[9],
																  packetInfo->ip6_header->src[10],packetInfo->ip6_header->src[11],
																  packetInfo->ip6_header->src[12],packetInfo->ip6_header->src[13],
																  packetInfo->ip6_header->src[14],packetInfo->ip6_header->src[15]);
																  
		sprintf(dst_ip,"%x%x:%x%x:%x%x:%x%x:%x%x:%x%x:%x%x:%x%x", packetInfo->ip6_header->dst[0],packetInfo->ip6_header->dst[1],
																  packetInfo->ip6_header->dst[2],packetInfo->ip6_header->dst[3],
																  packetInfo->ip6_header->dst[4],packetInfo->ip6_header->dst[5],
																  packetInfo->ip6_header->dst[6],packetInfo->ip6_header->dst[7],
																  packetInfo->ip6_header->dst[8],packetInfo->ip6_header->dst[9],
																  packetInfo->ip6_header->dst[10],packetInfo->ip6_header->dst[11],
																  packetInfo->ip6_header->dst[12],packetInfo->ip6_header->dst[13],
																  packetInfo->ip6_header->dst[14],packetInfo->ip6_header->dst[15]);			
	}
	
	char src_port[PORT_LENGTH],dst_port[PORT_LENGTH];
	
	//stores source and destination port number in char array.
	uint16_t src_port_num = packetInfo->tcp_header->tcph_srcport;
	sprintf(src_port,"%u",src_port_num);
	
	uint16_t dst_port_num = packetInfo->tcp_header->tcph_destport;
	sprintf(dst_port,"%u",dst_port_num);
	
	//combines ip and port num to create session id.
	*session_id = src_ip;
	*session_id += ":";
	*session_id += src_port;
	*session_id += ",";
	*session_id += dst_ip;
	*session_id += ":";
	*session_id += dst_port;
}


   /*
	*Utility Functions
	*---------------------------------------------------
	* clearSet : clears the content of the set at the end of file.
	*/
void sslParser :: clearSet(){
	g_session_id_set.clear();
}


   /*
	*Utility Function
	*---------------------------------------------------
	* parseClientHello : Parses the Client hello packet 
	*
	* sslInfo*	:	Struct which contains information regarding single TLS packet
	*
	* Return Type :	0 , if Client hello packet with no server name
	*				1 , if Client hello packet with partial server name
	*				2 , if Client hello packet with full server name
	*/
int sslParser :: parseClientHello(struct sslInfo* sslPacketInfo){
	
	uint16_t tls_ver = ((uint16_t)m_buff_ptr[m_bytesRead+4 ]<<8 | (uint16_t)m_buff_ptr[m_bytesRead+5]<<0);

	if((tls_ver == TLS_V1_0 || tls_ver == TLS_V1_1 || tls_ver == TLS_V1_2 || tls_ver == TLS_V1_3)){

		//skipping client hello header.
		m_bytesRead += 6 ;

		//skipping random 32 bytes
		m_bytesRead += 32;

		//skipping session ID length.
		VERIFY_CURRENT_BYTE_OF_PACKET();
		m_bytesRead += m_buff_ptr[m_bytesRead]+1;

		// reading and skipping cipher suite length
		_VERIFY_NEXT_BYTE_OF_PACKET(1);
		uint16_t cipher_suit_length = ((uint16_t)m_buff_ptr[m_bytesRead]<<8 | (uint16_t)m_buff_ptr[m_bytesRead+1]<<0);
		m_bytesRead += (uint32_t)cipher_suit_length + 2;

		//skipping compression methods
		m_bytesRead += 2;

		_VERIFY_NEXT_BYTE_OF_PACKET(1);
		uint16_t extension_length = ((uint16_t)m_buff_ptr[m_bytesRead]<<8 | (uint16_t)m_buff_ptr[m_bytesRead+1]<<0);

		//skipping extension length field
		m_bytesRead += 2;

		if(extension_length == 0){
			//printf("does not contain server name\n");
			return 0;
		}

		uint16_t trackLength = extension_length;
		uint16_t ext_type;
		uint16_t ext_len;
		while(trackLength>0 && m_bytesRead <= m_ssl_payload_length){
			
			_VERIFY_NEXT_BYTE_OF_PACKET(3);
			ext_type = ((uint16_t)m_buff_ptr[m_bytesRead]<<8 | (uint16_t)m_buff_ptr[m_bytesRead+1]<<0);
			ext_len  = ((uint16_t)m_buff_ptr[m_bytesRead+2]<<8 | (uint16_t)m_buff_ptr[m_bytesRead+3]<<0);

			m_bytesRead += 4;
			trackLength -= 4;

			if(ext_type == 0){
				m_bytesRead += 3;
				trackLength -= 3;
			
				_VERIFY_NEXT_BYTE_OF_PACKET(1);
				uint16_t server_name_len = ((uint16_t)m_buff_ptr[m_bytesRead]<<8 | (uint16_t)m_buff_ptr[m_bytesRead+1]<<0);
				//	printf("servername length : %u\n",server_name_len);
				
				if(server_name_len == 0){
					return 0;
				}

				//storing the server name legnth value in struct.
				sslPacketInfo->server_name_length = server_name_len;

				m_bytesRead += 2;
				trackLength -= 2;

				if(server_name_len > (m_ssl_payload_length - m_bytesRead)){
					//printf("partial server name found\n");
					return 1;
				}

				//unsigned char server_name[server_name_len+1];
				sslPacketInfo->server_name = (unsigned char*)(m_buff_ptr + m_bytesRead);
				return 2; // server name mil gya.
			}
			else{
				m_bytesRead += ext_len;
				trackLength -= ext_len;
			}
		}
	}
	//doesnt contain server name.
	return 0;
}


   /*
	*Utility Function
	*---------------------------------------------------
	* parseServerHello : Parses the Server hello packet 
	*
	* sslInfo*	:	Struct which contains information regarding single TLS packet
	*
	* Return Type :	3 , if Server hello packet with Certificate
	*				4 , Server hello packet without Certificate
	*/
int sslParser :: parseServerHello(struct sslInfo* sslPacketInfo, struct pcapPacketInfo* packetInfo){

	//skipping handshake type field.
	m_bytesRead += 1;	

	//reading and skipping server hello length.
	uint32_t server_hello_length = ((uint32_t)m_buff_ptr[m_bytesRead]<<16 | (uint32_t)m_buff_ptr[m_bytesRead+1]<<8 | (uint32_t)m_buff_ptr[m_bytesRead+2]<<0);

	//skipping server hello length field.
	m_bytesRead += 3;
	
	_VERIFY_NEXT_BYTE_OF_PACKET(1);
	uint16_t tls_ver = ((uint16_t)m_buff_ptr[m_bytesRead]<<8 | (uint16_t)m_buff_ptr[m_bytesRead+1]<<0);
	if((tls_ver == TLS_V1_0 || tls_ver == TLS_V1_1 || tls_ver == TLS_V1_2 || tls_ver == TLS_V1_3)){

		//skipping version(2 bytes) and random(32 bytes).
		m_bytesRead += 34;

		// reading session id length
		int session_id_len = m_buff_ptr[m_bytesRead];

		// skipping session id field ,Cipher suite and compression method.
		m_bytesRead += (4 + session_id_len);
		
		_VERIFY_NEXT_BYTE_OF_PACKET(1);
		uint16_t extension_len = ((uint16_t)m_buff_ptr[m_bytesRead]<<8 | (uint16_t)m_buff_ptr[m_bytesRead+1]<<0);

		//reading and skipping extension lengh.
		m_bytesRead += 2 + extension_len;

		if(m_ssl_payload_length == server_hello_length + 9){
				string l_curr_session_id ;
				createSessionId(&l_curr_session_id,packetInfo);
				g_session_id_set.insert(l_curr_session_id);
				return INT_MAX;
		}

		//check if again record layer.
		VERIFY_CURRENT_BYTE_OF_PACKET();
		if(m_buff_ptr[m_bytesRead]==HANDSHAKE){
			// skipping record layer header and validates the m_bytesRead value.
			m_bytesRead += 5;
			
			VERIFY_CURRENT_BYTE_OF_PACKET();
			if(m_buff_ptr[m_bytesRead]== CERTIFICATE){
				//certificate found in this packet
				return 3;
			}
			else{	
				// certificate not found
				return 4;
			}
		}
		
		else if(m_buff_ptr[m_bytesRead]== CERTIFICATE){
			//certificate found in this packet
			return 3;
		}
		else{
			// certificate not found
			return 4;
		}
	}
	//printf("no certificate found\n\n");
	return 4;
}


	   /*
		* Interface parseSslPacket
		*---------------------------------------------------
		* Summary : interface of the class, through which user can pass values
		*
		* buff	: pointer to the buffer containing bytes of the file
		*
		* payloadLength : length of the payload to be read
		*
		* sslInfo 	: struct to store details of the TLS packet
		*
		* Return Status Code : -1 if packet not Parsed
		*					    0 if Client hello packet with no server name
		* 						1 if Client hello packet with partial server name
		* 						2 if Client hello packet with full server name
		* 						3 if Server hello packet with Certificate
		* 						4 if Server hello packet without Certificate
		*/
int sslParser :: parseSslPacket(unsigned char* buff , int payloadLength , struct sslInfo* sslPacketInfo, struct pcapPacketInfo* packetInfo){
	
	m_buff_ptr = buff;
	m_ssl_payload_length = payloadLength;
	m_bytesRead = 0;

	if(m_ssl_payload_length == 0){
		return -1;
	}

	uint16_t tls_ver = ((uint16_t)m_buff_ptr[1]<<8 | (uint16_t)m_buff_ptr[2]<<0);	

	if(m_buff_ptr[0] == HANDSHAKE && (tls_ver == TLS_V1_0 || tls_ver == TLS_V1_1 || tls_ver == TLS_V1_2 || tls_ver == TLS_V1_3)){
		uint16_t record_layer_payload_length = ((uint16_t)m_buff_ptr[3]<<8 | (uint16_t)m_buff_ptr[4]<<0);

		bool frag_or_not = false;

		//checks for fragmentation and set the boolean variable frag_or_not.
		if(m_ssl_payload_length < (record_layer_payload_length+5)){
			frag_or_not = true;
		}

		//sets bit if packets is fragmented.
		sslPacketInfo->frag_or_not = frag_or_not;

		// skips the record layer header.		
		m_bytesRead += 5;
		
		VERIFY_CURRENT_BYTE_OF_PACKET();
		
		//checking for client hello packet.
		if(m_buff_ptr[m_bytesRead] == CLIENT_HELLO){
			int packetStatus = parseClientHello(sslPacketInfo);
			return packetStatus;
		}
		
		//checking for server hello packet
		else if( m_buff_ptr[m_bytesRead]== SERVER_HELLO) {
			int packetStatus = parseServerHello(sslPacketInfo , packetInfo);
			return packetStatus;
		}
		
		//checking for certificate.
		else if ( m_buff_ptr[m_bytesRead] == CERTIFICATE ){
			string l_curr_session_id ;
			createSessionId(&l_curr_session_id,packetInfo);
			set<string>::iterator itr = g_session_id_set.find(l_curr_session_id);
			
			if(itr == g_session_id_set.end()){
				return 4;
			}
			else{
				return 5;
			}
		}
	}


	// if not tls then return -1
	return -1;
}